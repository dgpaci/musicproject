Music Project
=============

Music Project è un progetto realizzato a scopo didattico con Genropy. 

È il risultato della creazione di un applicativo tramite importazione di un database SQL con il Package Editor. Per l'operazione di importazione si faccia riferimento all'articolo di blog presente a questo link: https://www.genropy.org/blog/dal-database-alla-web-app-in-meno-di-2-minuti/ 

Per avviare Music Project:

1) avvia in una finestra di terminale gnrdaemon

2) in un'altra finestra di terminale, lancia la `gnrdbsetup music_project`

3) avvia l'applicativo con `gnrwsgiserve musicproject --restore {database_path}`, sostituendo {database_path} con il percorso corrispondente al file musicproject_20201113_1747.zip contenuto nella cartella *data* del repository.

N.B. Nel corso delle modifiche, sono state modificate le primary key delle tabelle, questo al fine di utilizzare tool disponibili solo con le chiavi predefinite di Genropy.