#!/usr/bin/env python
# encoding: utf-8
from gnr.app.gnrdbo import GnrDboTable, GnrDboPackage

class Package(GnrDboPackage):
    def config_attributes(self):
        return dict(comment='mu package',sqlschema='mu',sqlprefix=True,
                    name_short='Mu', name_long='Music', name_full='Mu')
                    
    def config_db(self, pkg):
        pass
        
class Table(GnrDboTable):
    pass
