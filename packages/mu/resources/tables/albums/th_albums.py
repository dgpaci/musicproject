#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('albumid')
        r.fieldcell('title')
        r.fieldcell('artistid')

    def th_order(self):
        return 'albumid'

    def th_query(self):
        return dict(column='title', op='contains', val='')



class Form(BaseComponent):

    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.contentPane(region='top',datapath='.record')
        fb = top.formbuilder(cols=2, border_spacing='4px')
        fb.field('albumid')
        fb.field('title')
        fb.field('artistid')
        center = bc.contentPane(region='center')

        #center.borderTableHandler(relation='@mu_tracks_albumid',       
        #center.stackTableHandler(relation='@mu_tracks_albumid', 
        #center.inlineTableHandler(relation='@mu_tracks_albumid', 
        #center.plainTableHandler(relation='@mu_tracks_albumid',        
        #center.paletteTableHandler(relation='@mu_tracks_albumid', 
        center.dialogTableHandler(relation='@mu_tracks_albumid', 
                        #virtualStore=True,                             / Disabilita la ricerca
                        #grid_multiSelect=False,                        / Impedisce la selezione di celle multiple
                        #grid_selfDragRows=True,                        / Permette il trascinamento delle celle per riordinarle
                        #extendedQuery=True,                            / Attiva la ricerca avanzata
                        #addrow=False, delrow=False,                    / Disabilita l'inserimento e l'aggiunta di nuovi record
                        #export=True                                    / Attiva l'esportazione in Excel
                        )


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px' )
