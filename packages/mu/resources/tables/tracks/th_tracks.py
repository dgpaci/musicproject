#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method, metadata

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('trackid')
        r.fieldcell('name')
        r.fieldcell('albumid')
        r.fieldcell('mediatypeid')
    #    r.fieldcell('genreid')
    #   Modificato con ID di default aggiunto con gerarchica
        r.fieldcell('genre_id')
        r.fieldcell('composer')
        r.fieldcell('milliseconds')
        r.fieldcell('bytes')
        r.fieldcell('unitprice')

    #ALTERNATIVE STRUCT: aggiunta colonna Artista per nome
    def th_struct_artists(self,struct):
        "View con Artisti"
        r = struct.view().rows()
        r.fieldcell('name')
        r.fieldcell('albumid')
        r.fieldcell('@albumid.@artistid.name', name='Artist')

    def th_order(self):
        return 'trackid'

    def th_query(self):
        return dict(column='name', op='contains', val='')

#ALTERNATIVE VIEW per Artista: uso di runOnStart, widget e order
class ViewArtist(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('@albumid.@artistid.name', name='Artist')
        r.fieldcell('name', name='Track')
        r.fieldcell('albumid', name='Album')

    def th_struct_long(self,struct):
        r = struct.view().rows()
        r.fieldcell('@albumid.@artistid.name', name='Artist')
        r.fieldcell('name', name='Track')
        r.fieldcell('albumid', name='Album')
        r.fieldcell('milliseconds', name='Length')
        r.fieldcell('bytes', name='Bytes')

    def th_order(self):
        return '@albumid.@artistid.name, name'

    def th_query(self):
        return dict(column='name', op='contains', val='', runOnStart=True)
    
    def th_options(self):
        return dict(widget='dialog', readOnly=True)

    #SECTIONS: sections automatiche per genere, successivamente limitate per numero
    def th_top_toolbarsuperiore(self,top):
        top.slotToolbar('*,sections@genre_id,10', childname='superiore', _position='<bar', sections_genre_id_multiButton=False)
        
    #SECTIONS: sections automatiche per tipo media e manuale per durata
    def th_bottom_toolbarinferiore(self,bottom):
        bottom.slotToolbar('10,sections@length,*,sections@mediatypeid,10', childname='inferiore')
    
    #SECTIONS: definizione della section "length" usata nella toolbar inferiore
    @metadata(variable_struct=True)
    def th_sections_length(self):
        return [dict(code='tutti', caption='Tutti'),
                dict(code='1min', caption='<1min', condition='$milliseconds < 100000'),
                dict(code='2min', caption='<2min', condition='$milliseconds >= 100000 AND $milliseconds <200000'),
                dict(code='3min', caption='<3min', condition='$milliseconds >= 200000 AND $milliseconds <300000'),
                dict(code='4min', caption='>3min', condition='$milliseconds >= 300000', struct='long')]

    #QUERY BY SAMPLE / Artista e Genere: caso semplice, campo testuale
    #def th_queryBySample(self):
    #    return dict(fields=[dict(field='@albumid.@artistid.name', lbl='Artist', width='20em'),
    #                        dict(field='@genreid.name', lbl='Genre', width='20em')],
    #                        cols=2, isDefault=True)

    #QUERY BY SAMPLE / Artista e Genere: dbselect
    #def th_queryBySample(self):
    #    return dict(fields=[dict(field='@albumid.artistid', lbl='Artist', width='20em',
    #                        tag='dbselect',
    #                        table='mu.artists'),
    #                        dict(field='genreid', lbl='Genre', width='20em',
    #                        tag='dbselect',
    #                        dbtable='mu.genres',
    #                        hasDownArrow=True)],
    #                        cols=2, isDefault=True)

    #QUERY BY SAMPLE / Artista e Genere: checkbox, poi sostituita con section
    def th_queryBySample(self):
        return dict(fields=[dict(field='@albumid.artistid', lbl='Artist', width='20em',
                            tag='dbselect',
                            table='mu.artists'),
                            dict(field='name', lbl='Name')],
                        #    dict(field='@genreid.genreid', lbl='Genre', width='100em',
                        #   Modificato con ID di default aggiunto con gerarchica
                        #    dict(field='$genre_id', lbl='Genre', width='100em',
                        #    tag='checkboxtext',
                        #    table='mu.genres',
                        #    popup=True,
                        #    popup=False,
                        #    cols=8)],
                            cols=2, isDefault=True)

class Form(BaseComponent):

    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.contentPane(region='top',datapath='.record')
        fb = top.formbuilder(cols=2, border_spacing='4px')
        fb.field('trackid')
        fb.field('name')
        fb.field('albumid')
        fb.field('mediatypeid')
    #    fb.field('@genreid.name')
    #   Modificato con ID di default aggiunto con gerarchica
        fb.field('genre_id')    
        fb.field('composer')
        fb.field('milliseconds')
        fb.field('bytes')
        fb.field('unitprice')
        tc = bc.tabContainer(region='center',margin='2px')
        tab_mu_invoice_items_trackid = tc.contentPane(title='tracks')
        tab_mu_invoice_items_trackid.dialogTableHandler(relation='@mu_invoice_items_trackid')
        tab_mu_playlist_track_trackid = tc.contentPane(title='tracks')
        tab_mu_playlist_track_trackid.dialogTableHandler(relation='@mu_playlist_track_trackid')


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px' )

#ALTERNATIVE FORM: uso di readOnly, modal e bottoni
class FormPurchase(BaseComponent):

    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=1, border_spacing='4px')
        fb.field('name')
        fb.field('albumid')
        fb.field('mediatypeid')
    #    fb.field('@genreid.name')
    #   Modificato con ID di default aggiunto con gerarchica
        fb.field('genre_id')
        fb.field('unitprice')
        fb.button('Buy', action='alert("Good choice!")')
        fb.button('Do NOT Buy', action='this.form.abort()')

    def th_options(self):
        return dict(readOnly=True, modal=True)