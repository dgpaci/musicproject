#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('name')

    def th_order(self):
        return 'name'

    def th_query(self):
        return dict(column='name', op='contains', val='')



class Form(BaseComponent):

    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.contentPane(region='top',datapath='.record')
        fb = top.formbuilder(cols=2, border_spacing='4px')
        fb.field('name')
        center = bc.contentPane(region='center')
    #    center.dialogTableHandler(relation='@mu_tracks_genreid')
    #   Modificato con ID di default aggiunto con gerarchica
        th = center.plainTableHandler(relation='@tracks_by_genre')
        form.htree.relatedTableHandler(th,                          # / RELATEDTablehandler per tabelle gerarchiche
                                    dropOnRoot=False,               # / Impedisce l'assegnazione alla categoria "root"
                                    inherited=True)                 # / In caso di gerarchia a più livelli, i livelli superiori vedono il contenuto degli inferiori

    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px', hierarchical=True) 
        #Aggiunto HIERARCHICAL=TRUE per rendere la tabella gerarchica
