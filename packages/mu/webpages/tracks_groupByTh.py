# -*- coding: utf-8 -*-
            
class GnrCustomWebPage(object):
    py_requires="""th/th:TableHandler"""

    def main(self,root,**kwargs):
        bc = root.borderContainer(height='100%', datapath='main.tracks')
        bc.contentPane(region='top', margin='5px').dbSelect(value='^artist', table='mu.artists', lbl='Seleziona un artista')
        bc.contentPane(region='center').groupByTableHandler(table='mu.tracks', 
                                    struct=self.structAlbum, 
                                    condition='@albumid.artistid=:artist', condition_artist='^artist')

    def structAlbum(self, struct):
        r = struct.view().rows()
        r.fieldcell('@albumid.albumid', hidden=True)
        r.fieldcell('@albumid.title', name='album')
        r.fieldcell('name')
        r.fieldcell('genre_id')
        r.fieldcell('composer')