# -*- coding: utf-8 -*-
            
class GnrCustomWebPage(object):
    py_requires="""th/th:TableHandler"""

    def main(self,root,**kwargs):
        root.thFormHandler(table='mu.genres', datapath='main.genres')