#!/usr/bin/python3
# -*- coding: utf-8 -*-

def config(root,application=None):
    mu = root.branch('Music')
    mu.thpage('Albums',table='mu.albums')
    mu.thpage('Artists',table='mu.artists')
    mu.thpage('Customers',table='mu.customers')
    mu.thpage('Employees',table='mu.employees')
    mu.thpage('Genres',table='mu.genres')
    mu.thpage('Invoice items',table='mu.invoice_items')
    mu.thpage('Invoices',table='mu.invoices')
    mu.thpage('Playlist track',table='mu.playlist_track')
    mu.thpage('Playlists',table='mu.playlists')
    mu.thpage('Tracks',table='mu.tracks')
    mu.thpage('Tracks and Artists',table='mu.tracks', viewResource='ViewArtist', formResource='FormPurchase')