# encoding: utf-8


class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('tracks',legacy_name='main.tracks',legacy_db='chinook',pkey='trackid',caption_field='name',name_long='tracks',name_plural='tracks')
        tbl.column('trackid',dtype='I',name_long='trackid',legacy_name='TrackId')
        tbl.column('name',size='0:200',name_long='name',legacy_name='Name')
        tbl.column('albumid',dtype='I',name_long='albumid',legacy_name='AlbumId',indexed=True,unique=False
                    ).relation('albums.albumid',onDelete='raise', meta_thmode='dialog')
        tbl.column('mediatypeid',dtype='I',name_long='mediatypeid',legacy_name='MediaTypeId',indexed=True,unique=False
                    ).relation('media_types.mediatypeid',onDelete='raise', meta_thmode='dialog')
    #    tbl.column('genreid',dtype='I',name_long='genreid',legacy_name='GenreId', indexed=True,unique=False
    #                ).relation('genres.genreid', onDelete='raise', meta_thmode='dialog')
    #   Modificato con ID di default aggiunto con gerarchica
        tbl.column('genre_id',size='22', name_long='genreid', batch_assign=True
                    ).relation('genres.id', onDelete='raise', mode='foreign_key', relation_name='tracks_by_genre')
        tbl.column('composer',size='0:220',name_long='composer',legacy_name='Composer')
        tbl.column('milliseconds',dtype='I',name_long='milliseconds',legacy_name='Milliseconds')
        tbl.column('bytes',dtype='I',name_long='bytes',legacy_name='Bytes')
        tbl.column('unitprice',dtype='N',size='10,2',name_long='unitprice',legacy_name='UnitPrice')
