# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl =  pkg.table('employees',legacy_name='main.employees',legacy_db='chinook',pkey='employeeid',caption_field='lastname',name_long='employees',name_plural='employees')
        tbl.column('employeeid',dtype='I',name_long='employeeid',legacy_name='EmployeeId')
        tbl.column('lastname',size='0:20',name_long='lastname',legacy_name='LastName')
        tbl.column('firstname',size='0:20',name_long='firstname',legacy_name='FirstName')
        tbl.column('title',size='0:30',name_long='title',legacy_name='Title')
        tbl.column('reportsto',dtype='I',name_long='reportsto',legacy_name='ReportsTo',indexed=True,unique=False).relation('employees.employeeid',onDelete='raise', meta_thmode='dialog')
        tbl.column('birthdate',dtype='DH',name_long='birthdate',legacy_name='BirthDate')
        tbl.column('hiredate',dtype='DH',name_long='hiredate',legacy_name='HireDate')
        tbl.column('address',size='0:70',name_long='address',legacy_name='Address')
        tbl.column('city',size='0:40',name_long='city',legacy_name='City')
        tbl.column('state',size='0:40',name_long='state',legacy_name='State')
        tbl.column('country',size='0:40',name_long='country',legacy_name='Country')
        tbl.column('postalcode',size='0:10',name_long='postalcode',legacy_name='PostalCode')
        tbl.column('phone',size='0:24',name_long='phone',legacy_name='Phone')
        tbl.column('fax',size='0:24',name_long='fax',legacy_name='Fax')
        tbl.column('email',size='0:60',name_long='email',legacy_name='Email')