# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl =  pkg.table('playlist_track',legacy_name='main.playlist_track',legacy_db='chinook',pkey='_multikey',caption_field='_multikey',name_long='playlist_track',name_plural='playlist_track')
        tbl.column('playlistid',dtype='I',name_long='playlistid',legacy_name='PlaylistId').relation('playlists.playlistid',onDelete='raise', meta_thmode='dialog')
        tbl.column('trackid',dtype='I',name_long='trackid',legacy_name='TrackId',indexed=True,unique=False).relation('tracks.trackid',onDelete='raise', meta_thmode='dialog')
        tbl.column('_multikey',name_long='_multikey')