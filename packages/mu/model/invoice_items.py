# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl =  pkg.table('invoice_items',legacy_name='main.invoice_items',legacy_db='chinook',pkey='invoicelineid',caption_field='invoicelineid',name_long='invoice_items',name_plural='invoice_items')
        tbl.column('invoicelineid',dtype='I',name_long='invoicelineid',legacy_name='InvoiceLineId')
        tbl.column('invoiceid',dtype='I',name_long='invoiceid',legacy_name='InvoiceId',indexed=True,unique=False).relation('invoices.invoiceid',onDelete='raise', meta_thmode='dialog')
        tbl.column('trackid',dtype='I',name_long='trackid',legacy_name='TrackId',indexed=True,unique=False).relation('tracks.trackid',onDelete='raise', meta_thmode='dialog')
        tbl.column('unitprice',dtype='N',size='10,2',name_long='unitprice',legacy_name='UnitPrice')
        tbl.column('quantity',dtype='I',name_long='quantity',legacy_name='Quantity')