# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        #CHANGED PKEY INTO LASTNAME 
        tbl =  pkg.table('customers',legacy_name='main.customers',legacy_db='chinook',pkey='customerid',caption_field='lastname',name_long='customers',name_plural='customers')
        tbl.column('customerid',dtype='I',name_long='customerid',legacy_name='CustomerId')
        tbl.column('firstname',size='0:40',name_long='firstname',legacy_name='FirstName')
        tbl.column('lastname',size='0:20',name_long='lastname',legacy_name='LastName')
        tbl.column('company',size='0:80',name_long='company',legacy_name='Company')
        tbl.column('address',size='0:70',name_long='address',legacy_name='Address')
        tbl.column('city',size='0:40',name_long='city',legacy_name='City')
        tbl.column('state',size='0:40',name_long='state',legacy_name='State')
        tbl.column('country',size='0:40',name_long='country',legacy_name='Country')
        tbl.column('postalcode',size='0:10',name_long='postalcode',legacy_name='PostalCode')
        tbl.column('phone',size='0:24',name_long='phone',legacy_name='Phone')
        tbl.column('fax',size='0:24',name_long='fax',legacy_name='Fax')
        tbl.column('email',size='0:60',name_long='email',legacy_name='Email')
        tbl.column('supportrepid',dtype='I',name_long='supportrepid',legacy_name='SupportRepId',indexed=True,unique=False).relation('employees.employeeid',onDelete='raise', meta_thmode='dialog')