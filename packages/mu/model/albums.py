# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl =  pkg.table('albums',legacy_name='main.albums',legacy_db='chinook',pkey='albumid',
                            caption_field='title',name_long='albums',name_plural='albums')
        tbl.column('albumid',dtype='I',name_long='albumid',legacy_name='AlbumId')
        tbl.column('title',size='0:160',name_long='title',legacy_name='Title')
        tbl.column('artistid',dtype='I',name_long='artistid',legacy_name='ArtistId',indexed=True,unique=False).relation('artists.artistid',onDelete='raise', meta_thmode='dialog')