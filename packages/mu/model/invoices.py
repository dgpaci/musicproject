# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl =  pkg.table('invoices',legacy_name='main.invoices',legacy_db='chinook',pkey='invoiceid',caption_field='billingaddress',name_long='invoices',name_plural='invoices')
        tbl.column('invoiceid',dtype='I',name_long='invoiceid',legacy_name='InvoiceId')
        tbl.column('customerid',dtype='I',name_long='customerid',legacy_name='CustomerId',indexed=True,unique=False).relation('customers.customerid',onDelete='raise', meta_thmode='dialog')
        #CHANGED INVOICE DATE FORMAT INTO "DATE ONLY"
        tbl.column('invoicedate',dtype='D',name_long='invoicedate',legacy_name='InvoiceDate')
        tbl.column('billingaddress',size='0:70',name_long='billingaddress',legacy_name='BillingAddress')
        tbl.column('billingcity',size='0:40',name_long='billingcity',legacy_name='BillingCity')
        tbl.column('billingstate',size='0:40',name_long='billingstate',legacy_name='BillingState')
        tbl.column('billingcountry',size='0:40',name_long='billingcountry',legacy_name='BillingCountry')
        tbl.column('billingpostalcode',size='0:10',name_long='billingpostalcode',legacy_name='BillingPostalCode')
        tbl.column('total',dtype='N',size='10,2',name_long='total',legacy_name='Total')