# encoding: utf-8


class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('genres',legacy_name='main.genres',legacy_db='chinook',pkey='id',caption_field='name',
                            name_long='genres', name_plural='genres')
        self.sysFields(tbl, hierarchical=True, counter=True)
    #    tbl.column('genreid',dtype='I',name_long='genreid',legacy_name='GenreId')
    #   Modificato con ID di default aggiunto con gerarchica
        tbl.column('name',size='0:120',name_long='name',legacy_name='Name')
